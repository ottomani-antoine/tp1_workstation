# SELF-FOOTPRITING

## | HOST OS : 

### ~ Nom de la machine : 

*Input :*		
	
  	hostname

*Output :*

	ManjaroAntoine


### ~ OS et version :

*Input :*
	
  	uname -r

*Output :*

	5.8.16-2-MANJARO


### ~ Architecture processeur : 

*Input :*
	
  	lscpu | grep Architecture

*Output :*

	Architecture : x86_64


### ~ Quantité et modèle de la RAM : 

- Quantité : 

*Input :*

	inxi -xxx -m | grep total                                                

*Output :*

	RAM: total: 15.36 GiB used: 3.16 GiB (20.6%) 

- Modèle : 

*Input :*

	dmidecode --type 17 | grep -E 'Form Factor|Type'

*Output :*

	Form Factor: SODIMM
	Type: DDR4
	Type Detail: Synchronous


## | DEVICES 

### ~ Processeur :

- Marque/Modèle :

*Input :*

 	cat /proc/cpuinfo | grep -i "^model name" | awk -F": " '{print $2}' | head -1 | sed 's/ \+/ /g'

*Output :*

	Intel(R) Core(TM) i7-10510U CPU @ 1.80GHz

- Nombre de coeurs :

*Input :*

	cat /proc/cpuinfo | grep -i "^processor" | wc -l   

*Output :*

	8


==Explication du nom d'Intel :==
Dans mon cas : **"Intel(R) Core(TM) i7-10510U"**
- Intel Core désigne la marque c'est à dire pour quel type d'applications le processeur est destiné. On trouve les Intel Xeon, Core, Pentium, Céleron. Les Core sont préconisés pour la 3D, l'édition photo/vidéo, les jeux ect. 

- "i7" définit la famille des processeurs qui indique la performance relative. 

- "10" identifie la génération du processeur au sein d'une famille. 

- "510" sont les numéros d'identification du processeur qui servent à identifier les différentes caractéristiques d'un processeur comme la fréquence d'horloge de base, la fréquence maximale, le nombre de coeurs et de threads. 

- "U" permet de savoir pour quel système le processeur se destine : ordinateur de bureau, portable, appareil mobile ect.. 

### ~ Touchpad (marque/modèle) :

*Input :* 

	cat /proc/bus/input/devices | grep Touchpad                                                                                                                                                                 
*Output :*

	N: Name="SYNA7DB5:00 06CB:CD41 Touchpad"


### ~ Carte graphique (marque/modèle) :

*Input :*

	lspci|grep VGA

*Output :*

	00:02.0 VGA compatible controller: Intel Corporation UHD Graphics (rev 02)


### ~ Disques : 

#### ~ Marque et modèle :

*Input :* 

	sudo fdisk -l | grep Modèle  

*Output :*
	
  	Modèle de disque : Samsung SSD 860 

####  ~ Partitions :

*Input :*

	sudo fdisk -l | grep /dev/sda  

*Output :*

    /dev/sda1          4096     618495    614400   300M Système EFI
    /dev/sda2        618496  977180995 976562500 465,7G Système de fichiers Linux
    /dev/sda3    1935065126 1953520064  18454939   8,8G Partition d'échange Linux
    /dev/sda4     977182720 1367805951 390623232 186,3G Système de fichiers Linux
    /dev/sda5    1367805952 1935065087 567259136 270,5G Système de fichiers Linux




## Système de fichiers et rôles des partitions 

/dev/sda1 = Partition EFI pour le démarrage du disque GPT en UEFI. Elle est formatée en Fat32. 
/dev/sda2 = Formatée en ext4. Contient toutes les données système et utilisateur de Manjaro. 
/dev/sda3 = Partiton de swap qui est la mémoire virtuelle du système. 
/dev/sda4 = Partition en ext4 qui pour l'instant ne me sert pas. 
/dev/Sda5 = Partition formatée en ext4 sur sont les données système et utilisateur de Kali Linux. Cette partition interragit avec /dev/sda1 qui sert d'amorçage pour Kali. 



## | USERS 

### ~ Liste des users : 

*Input :*
	
  	cat /etc/passwd | awk -F: '{print $ 1}'  

*Output :*
                                          
    root
    nobody
    dbus
    bin
    daemon
    mail
    ftp
    http
    systemd-journal-remote
    systemd-network
    systemd-resolve
    systemd-timesync
    systemd-coredump
    uuidd
    dhcpcd
    dnsmasq
    rpc
    avahi
    colord
    gdm
    geoclue
    git
    nm-openconnect
    nm-openvpn
    ntp
    polkitd
    rtkit
    usbmux
    antoine
    cups
    spamd

### ~ Full admin :

*Input :*
	
  	cat /etc/group | grep root                                                        

*Output :*

	root:x:0:root


## | PROCESSUS 

### ~ Liste des processus : 

*Input :* 

	pstree

*Output :*
                                                                                                                                                                                                      
	systemd─┬─ModemManager───2*[{ModemManager}]
        ├─NetworkManager───2*[{NetworkManager}]
        ├─accounts-daemon───2*[{accounts-daemon}]
        ├─bluetoothd
        ├─colord───2*[{colord}]
        ├─dbus-daemon
        ├─gdm─┬─gdm-session-wor─┬─gdm-wayland-ses─┬─gnome-session-b───3*[{gnome-session-b}]
        │     │                 │                 └─2*[{gdm-wayland-ses}]
        │     │                 └─2*[{gdm-session-wor}]
        │     └─2*[{gdm}]
        ├─gnome-keyring-d───3*[{gnome-keyring-d}]
        ├─lvmetad
        ├─pamac-daemon───2*[{pamac-daemon}]
        ├─polkitd───11*[{polkitd}]
        ├─rtkit-daemon───2*[{rtkit-daemon}]
        ├─snapd───14*[{snapd}]
        ├─systemd─┬─(sd-pam)
        │         ├─at-spi-bus-laun─┬─dbus-daemon
        │         │                 └─3*[{at-spi-bus-laun}]
        │         ├─at-spi2-registr───2*[{at-spi2-registr}]
        │         ├─chromium─┬─chromium───chromium───12*[{chromium}]
        │         │          ├─chromium───chromium─┬─chromium───22*[{chromium}]
        │         │          │                     ├─2*[chromium───18*[{chromium}]]
        │         │          │                     ├─chromium───15*[{chromium}]
        │         │          │                     ├─7*[chromium───12*[{chromium}]]
        │         │          │                     ├─chromium───14*[{chromium}]
        │         │          │                     ├─chromium───11*[{chromium}]
        │         │          │                     └─3*[chromium───17*[{chromium}]]
        │         │          ├─chromium───11*[{chromium}]
        │         │          ├─chromium───6*[{chromium}]
        │         │          └─28*[{chromium}]
        │         ├─code-oss─┬─electron───electron───13*[{electron}]
        │         │          ├─electron
        │         │          ├─electron───5*[{electron}]
        │         │          ├─electron─┬─electron───14*[{electron}]
        │         │          │          ├─electron───11*[{electron}]
        │         │          │          ├─zsh
        │         │          │          └─21*[{electron}]
        │         │          ├─electron───19*[{electron}]
        │         │          └─31*[{code-oss}]
        │         ├─dbus-daemon
        │         ├─dconf-service───2*[{dconf-service}]
        │         ├─evolution-addre───5*[{evolution-addre}]
        │         ├─evolution-calen───11*[{evolution-calen}]
        │         ├─evolution-sourc───3*[{evolution-sourc}]
        │         ├─gedit───3*[{gedit}]
        │         ├─gjs───10*[{gjs}]
        │         ├─gnome-session-b─┬─evolution-alarm───5*[{evolution-alarm}]
        │         │                 ├─gsd-disk-utilit───2*[{gsd-disk-utilit}]
        │         │                 ├─python3───2*[{python3}]
        │         │                 ├─tracker-miner-f───4*[{tracker-miner-f}]
        │         │                 └─3*[{gnome-session-b}]
        │         ├─gnome-session-c───{gnome-session-c}
        │         ├─gnome-shell─┬─Xwayland───20*[{Xwayland}]
        │         │             ├─ibus-daemon─┬─ibus-dconf───3*[{ibus-dconf}]
        │         │             │             ├─ibus-engine-sim───2*[{ibus-engine-sim}]
        │         │             │             ├─ibus-extension-───3*[{ibus-extension-}]
        │         │             │             └─2*[{ibus-daemon}]
        │         │             ├─pamac-manager───11*[{pamac-manager}]
        │         │             └─16*[{gnome-shell}]
        │         ├─gnome-shell-cal───5*[{gnome-shell-cal}]
        │         ├─gnome-terminal-─┬─zsh───pstree
        │         │                 └─3*[{gnome-terminal-}]
        │         ├─goa-daemon───3*[{goa-daemon}]
        │         ├─goa-identity-se───2*[{goa-identity-se}]
        │         ├─gsd-a11y-settin───3*[{gsd-a11y-settin}]
        │         ├─gsd-color───3*[{gsd-color}]
        │         ├─gsd-datetime───3*[{gsd-datetime}]
        │         ├─gsd-housekeepin───3*[{gsd-housekeepin}]
        │         ├─gsd-keyboard───3*[{gsd-keyboard}]
        │         ├─gsd-media-keys───4*[{gsd-media-keys}]
        │         ├─gsd-power───4*[{gsd-power}]
        │         ├─gsd-print-notif───2*[{gsd-print-notif}]
        │         ├─gsd-printer───2*[{gsd-printer}]
        │         ├─gsd-rfkill───2*[{gsd-rfkill}]
        │         ├─gsd-screensaver───2*[{gsd-screensaver}]
        │         ├─gsd-sharing───3*[{gsd-sharing}]
        │         ├─gsd-smartcard───4*[{gsd-smartcard}]
        │         ├─gsd-sound───3*[{gsd-sound}]
        │         ├─gsd-usb-protect───3*[{gsd-usb-protect}]
        │         ├─gsd-wacom───3*[{gsd-wacom}]
        │         ├─gsd-wwan───3*[{gsd-wwan}]
        │         ├─gsd-xsettings───3*[{gsd-xsettings}]
        │         ├─gvfs-afc-volume───3*[{gvfs-afc-volume}]
        │         ├─gvfs-goa-volume───2*[{gvfs-goa-volume}]
        │         ├─gvfs-mtp-volume───2*[{gvfs-mtp-volume}]
        │         ├─gvfs-udisks2-vo───3*[{gvfs-udisks2-vo}]
        │         ├─gvfsd─┬─gvfsd-dnssd───2*[{gvfsd-dnssd}]
        │         │       ├─gvfsd-ftp───3*[{gvfsd-ftp}]
        │         │       ├─gvfsd-network───3*[{gvfsd-network}]
        │         │       ├─gvfsd-trash───2*[{gvfsd-trash}]
        │         │       └─2*[{gvfsd}]
        │         ├─gvfsd-fuse───5*[{gvfsd-fuse}]
        │         ├─gvfsd-metadata───2*[{gvfsd-metadata}]
        │         ├─ibus-portal───2*[{ibus-portal}]
        │         ├─ibus-x11───2*[{ibus-x11}]
        │         ├─pulseaudio─┬─gsettings-helpe───3*[{gsettings-helpe}]
        │         │            └─2*[{pulseaudio}]
        │         └─teams─┬─teams─┬─teams───14*[{teams}]
        │                 │       ├─teams───19*[{teams}]
        │                 │       ├─teams───13*[{teams}]
        │                 │       └─teams───41*[{teams}]
        │                 ├─teams───9*[{teams}]
        │                 └─36*[{teams}]
        ├─systemd-journal
        ├─systemd-logind
        ├─systemd-udevd
        ├─udisksd───4*[{udisksd}]
        ├─upowerd───2*[{upowerd}]
        └─wpa_supplicant


==Expliquer l'utilité de 5 services sytème :==

- Network Manager : C'est l'outil de gestion des connexions réseaux. Son utilité est la création et la configuration des accès à Internet, LAN, VPN pour ne citer qu'eux. 

- Systemd : Gestionnaire de système, pièce principale de Linux. C'est le 1er programme lancé qui lance tous les autres afin d'avoir un système opérationnel. C'est lui qui arrête ou redémarre l'ordinateur par exemple. 

- gdm : C'est le gestionnaire de session, il gère le déverouillage et vérouillage de l'ordinateur. 

- polkitd : bibliothèque de logiciels qui permet aux applications s'exécutant avec des droits restreints d'interagir avec des services privilégiés du système. 

- udiskd :  Interface pour les périphériques système comme les disques durs ou clés usb. Il gère différentes opérations comme le montage, le démontage ou le formatage des périphériques système. 


### ~ Processus lancés par le full-admin :

*Input :*

	pstree root

*Output :*

	systemd─┬─ModemManager───2*[{ModemManager}]
        ├─NetworkManager───2*[{NetworkManager}]
        ├─accounts-daemon───2*[{accounts-daemon}]
        ├─bluetoothd
        ├─colord───2*[{colord}]
        ├─dbus-daemon
        ├─gdm─┬─gdm-session-wor─┬─gdm-wayland-ses─┬─gnome-session-b───3*[{gnome-session-b}]
        │     │                 │                 └─2*[{gdm-wayland-ses}]
        │     │                 └─2*[{gdm-session-wor}]
        │     └─2*[{gdm}]
        ├─gnome-keyring-d───3*[{gnome-keyring-d}]
        ├─lvmetad
        ├─pamac-daemon───2*[{pamac-daemon}]
        ├─polkitd───11*[{polkitd}]
        ├─rtkit-daemon───2*[{rtkit-daemon}]
        ├─snapd───14*[{snapd}]
        ├─systemd─┬─(sd-pam)
        │         ├─at-spi-bus-laun─┬─dbus-daemon
        │         │                 └─3*[{at-spi-bus-laun}]
        │         ├─at-spi2-registr───2*[{at-spi2-registr}]
        │         ├─chromium─┬─chromium───chromium───12*[{chromium}]
        │         │          ├─chromium───chromium─┬─chromium───22*[{chromium}]
        │         │          │                     ├─chromium───13*[{chromium}]
        │         │          │                     ├─chromium───18*[{chromium}]
        │         │          │                     ├─6*[chromium───12*[{chromium}]]
        │         │          │                     └─chromium───17*[{chromium}]
        │         │          ├─chromium───11*[{chromium}]
        │         │          ├─chromium───6*[{chromium}]
        │         │          └─28*[{chromium}]
        │         ├─code-oss─┬─electron───electron───13*[{electron}]
        │         │          ├─electron
        │         │          ├─electron───5*[{electron}]
        │         │          ├─electron─┬─electron───14*[{electron}]
        │         │          │          ├─electron───11*[{electron}]
        │         │          │          ├─zsh
        │         │          │          └─20*[{electron}]
        │         │          ├─electron───19*[{electron}]
        │         │          └─31*[{code-oss}]
        │         ├─dbus-daemon
        │         ├─dconf-service───2*[{dconf-service}]
        │         ├─evolution-addre───5*[{evolution-addre}]
        │         ├─evolution-calen───11*[{evolution-calen}]
        │         ├─evolution-sourc───3*[{evolution-sourc}]
        │         ├─gedit───3*[{gedit}]
        │         ├─gjs───10*[{gjs}]
        │         ├─gnome-session-b─┬─evolution-alarm───5*[{evolution-alarm}]
        │         │                 ├─gsd-disk-utilit───2*[{gsd-disk-utilit}]
        │         │                 ├─python3───2*[{python3}]
        │         │                 ├─tracker-miner-f───4*[{tracker-miner-f}]
        │         │                 └─3*[{gnome-session-b}]
        │         ├─gnome-session-c───{gnome-session-c}
        │         ├─gnome-shell─┬─Xwayland───20*[{Xwayland}]
        │         │             ├─ibus-daemon─┬─ibus-dconf───3*[{ibus-dconf}]
        │         │             │             ├─ibus-engine-sim───2*[{ibus-engine-sim}]
        │         │             │             ├─ibus-extension-───3*[{ibus-extension-}]
        │         │             │             └─2*[{ibus-daemon}]
        │         │             ├─pamac-manager───11*[{pamac-manager}]
        │         │             └─17*[{gnome-shell}]
        │         ├─gnome-shell-cal───5*[{gnome-shell-cal}]
        │         ├─gnome-terminal-─┬─zsh───sudo───su───bash───pstree
        │         │                 └─3*[{gnome-terminal-}]
        │         ├─goa-daemon───3*[{goa-daemon}]
        │         ├─goa-identity-se───2*[{goa-identity-se}]
        │         ├─gsd-a11y-settin───3*[{gsd-a11y-settin}]
        │         ├─gsd-color───3*[{gsd-color}]
        │         ├─gsd-datetime───3*[{gsd-datetime}]
        │         ├─gsd-housekeepin───3*[{gsd-housekeepin}]
        │         ├─gsd-keyboard───3*[{gsd-keyboard}]
        │         ├─gsd-media-keys───4*[{gsd-media-keys}]
        │         ├─gsd-power───4*[{gsd-power}]
        │         ├─gsd-print-notif───2*[{gsd-print-notif}]
        │         ├─gsd-printer───2*[{gsd-printer}]
        │         ├─gsd-rfkill───2*[{gsd-rfkill}]
        │         ├─gsd-screensaver───2*[{gsd-screensaver}]
        │         ├─gsd-sharing───3*[{gsd-sharing}]
        │         ├─gsd-smartcard───4*[{gsd-smartcard}]
        │         ├─gsd-sound───3*[{gsd-sound}]
        │         ├─gsd-usb-protect───3*[{gsd-usb-protect}]
        │         ├─gsd-wacom───3*[{gsd-wacom}]
        │         ├─gsd-wwan───3*[{gsd-wwan}]
        │         ├─gsd-xsettings───3*[{gsd-xsettings}]
        │         ├─gvfs-afc-volume───3*[{gvfs-afc-volume}]
        │         ├─gvfs-goa-volume───2*[{gvfs-goa-volume}]
        │         ├─gvfs-mtp-volume───2*[{gvfs-mtp-volume}]
        │         ├─gvfs-udisks2-vo───3*[{gvfs-udisks2-vo}]
        │         ├─gvfsd─┬─gvfsd-dnssd───2*[{gvfsd-dnssd}]
        │         │       ├─gvfsd-ftp───3*[{gvfsd-ftp}]
        │         │       ├─gvfsd-network───3*[{gvfsd-network}]
        │         │       ├─gvfsd-trash───3*[{gvfsd-trash}]
        │         │       └─2*[{gvfsd}]
        │         ├─gvfsd-fuse───5*[{gvfsd-fuse}]
        │         ├─gvfsd-metadata───2*[{gvfsd-metadata}]
        │         ├─ibus-portal───2*[{ibus-portal}]
        │         ├─ibus-x11───2*[{ibus-x11}]
        │         ├─nautilus───5*[{nautilus}]
        │         ├─pulseaudio─┬─gsettings-helpe───3*[{gsettings-helpe}]
        │         │            └─2*[{pulseaudio}]
        │         └─teams─┬─teams─┬─teams───14*[{teams}]
        │                 │       ├─teams───19*[{teams}]
        │                 │       ├─teams───13*[{teams}]
        │                 │       └─teams───41*[{teams}]
        │                 ├─teams───9*[{teams}]
        │                 └─36*[{teams}]
        ├─systemd-hostnam
        ├─systemd-journal
        ├─systemd-logind
        ├─systemd-udevd
        ├─udisksd───4*[{udisksd}]
        ├─upowerd───2*[{upowerd}]
        └─wpa_supplicant


## | NETWORK 

### ~ Cartes réseaux de la machine :

*Input :*

	ip a

*Output :*

	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
	2: enp1s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether b4:a9:fc:7a:25:e4 brd ff:ff:ff:ff:ff:ff
	3: wlp0s20f3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 58:96:1d:cd:c2:d8 brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.9/24 brd 192.168.1.255 scope global dynamic noprefixroute wlp0s20f3
       valid_lft 77182sec preferred_lft 77182sec
    inet6 fe80::c898:b996:d51:d0f9/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

==Expliquer leurs fonctions :==
- interface lo : Correspond à la carte de loopback qui permet de contacter la machine locale sans passer par une interface extérieure. Elle prend pour adresse celle du localhost. 

- interface enp1s0 : interface réseau Ethernet accessible par un câble Ethernet à connecteur RJ45. Elle permet soit de relier deux équipements en réseau soit d'accéder à Internet lorsque que le câble est relié à une Box. 

- interface wlp0s20f3 : interface réseau wifi qui permet la transmission de données numériques sans fil et permet de se connecter à Internet sans câbles.



### ~ Lister les ports TCP/UDP en utilisation : 

- TCP 

*Input :*

	netstat -at -p                                                          

*Output :*

    Active Internet connections (servers and established)
    Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
    tcp        0      1 aspireAnto:56230        xx-fbcdn-shv-01-c:https LAST_ACK    -                   
    tcp        0      0 aspireAnto:60838        151.101.120.157:https   ESTABLISHED 2662/chromium --typ 
    tcp        0      0 aspireAnto:41830        52.113.205.243:https    ESTABLISHED 1830/teams          
    tcp        0      0 aspireAnto:38887        52.113.199.227:https    ESTABLISHED 2247/537.36 --node- 
    tcp        0      0 aspireAnto:50290        wordpress.com:https     ESTABLISHED 2662/chromium --typ 
    tcp        0      0 aspireAnto:39946        cluster011.ovh.ne:https ESTABLISHED 2662/chromium --typ 
    tcp        0      0 aspireAnto:53378        192.0.73.2:https        ESTABLISHED 2662/chromium --typ 
    tcp        0      0 aspireAnto:35140        wn-in-f188.1e10:hpvroom ESTABLISHED 2662/chromium --typ 
    tcp        0      0 aspireAnto:41660        104.244.42.200:https    ESTABLISHED 2662/chromium --typ 
    tcp        0      0 aspireAnto:56016        52.114.75.53:https      ESTABLISHED 1830/teams          
    tcp        0      0 aspireAnto:58122        server-13-32-145-:https ESTABLISHED 2662/chromium --typ 


- UDP 

*Input :*

	netstat -au -p

*Output :*

    Active Internet connections (servers and established)
    Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
    udp        0      0 aspireAnto:59462        wq-in-f189.1e100.:https ESTABLISHED 2662/chromium --typ 
    udp        0      0 224.0.0.251:mdns        0.0.0.0:*                           2662/chromium --typ 
    udp        0      0 224.0.0.251:mdns        0.0.0.0:*                           2631/chromium       
    udp        0      0 aspireAnto:bootpc       bbox.lan:bootps         ESTABLISHED 540/NetworkManager  
    udp        0      0 aspireAnto:57755        i15-lef02-ix2-213:https ESTABLISHED 2662/chromium --typ 
    udp6       0      0 [::]:39712              [::]:*                              2247/537.36 --node- 
    udp6       0      0 [::]:56924              [::]:*                              2247/537.36 --node- 

                                                                                
  ==Identifier les programmes qui tournent et leurs fonctions==
  Les 3 principaux programmes qui tournent sur des ports UDP et TCP sont Teams, Google Chrome et Network Manager.
                                                                            
                                                                            
                                                                            
    



























