#!/bin/bash

# Autor : Antoine Ottomani
# Date : 23/10/20 
# This script describes the features of my O.S
# lists laptop's users and calculates ping latency

echo "Get average ping latency to 8.8.8.8 server..."
echo "Get average upload speed..."
echo -e "Get average download speed...\n" 


echo -e "*********************************************\n"


echo "OS :" `hostname` && echo "Linux Kernel :" `uname -r`
echo "Up since :" `who -b` && echo -e '\n'

# METTRE ICI LA MAJ DU SYSTEME 

echo -e "*********************************************\n" 


# GET NETWORK DATA
echo "Interface Wlp0s20f3 :" `ip route get 1.2.3.4 | awk '{print $7}'`

ping -c 3 google.com > ping.txt # ping 
echo "      8.8.8.8 average ping time: `cat ping.txt | grep avg | cut -f5 -d/` ms"


# Get Download & Upload Time
wget -o output.txt https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py 
echo "      average speed `cat output.txt | grep Upload`"
echo "      average speed `cat output.txt | grep Download`" && echo -e "\n"



echo -e "*********************************************\n"


inxi -xxx -m | grep total && echo -e "\n" # RAM 

echo -e "*********************************************\n"

# Disks
df -h /dev/sda5 > disk.txt 
echo "Disk :
Total : `cat disk.txt | grep /dev/sda5 | awk {'print $2'}`
Free : `cat disk.txt | grep /dev/sda5 | awk {'print $3'}`" && echo -e "\n"

echo -e "*********************************************\n"

echo "Users list :"
cut -f1 -d: /etc/passwd
