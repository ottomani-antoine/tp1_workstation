# Partage de fichiers

Voici un screen de mon bash qui prouve que la connexion ssh est établie depuis mon ordi vers CentOs. 

![ssh vers CentOs ](https://i.imgur.com/gfDc6X0.png)

Je ne considère pas pertinent de te mettre les screen des carte réseaux de CentOs puisque si elles ne sont pas allumées, je ne pourrais rien faire. 

## Démarche pour créer le serveur sur mon pc :

Tout d'abord il faut installer le paquet Samba sur son système, je fais donc 

    pacman -S samba

Une fois installé, il faut conigurer le serveur par le fichier "/etc/samba/smb.conf"
C'est dans ce fichier que seront définis les répertoires à partager et les autorisations par exemple. 

Le paquet Samba ne fournit pas ce fichier, il faut le créer avant de démarrer le serveur. 

Ce fichier est donc créer par : 

    nano /etc/samba/smb.conf

A partir de là, je me suis inspiré d'un modèle trouvé sur internet que j'ai adapté selon les besoins de l'exercice. 

![modification du fichier de config de samba](https://i.imgur.com/MLUBW0A.png)

J'ai donc 2 dossiers, dans mon répertoire personnel que je vais partager via smb. Il s'agit de TestSamba et Ynov. 

J'entre les commandes suivantes pour démarrer les services smb et nmb de manière automatique. 

![Démarrage des services](https://i.imgur.com/DTaI8Zb.png)


Une fois le serveur samba actif sur ma machine personnelle, je me connecte en ssh à la machine virtuelle pour installer le client samba qui permettra de monter les partages. 


![Installation du client samba](https://i.imgur.com/GD6mEhq.png)

Dans mon cas je l'avais déjà installé. 

A partir de là, je peux accéder à mes dossiers partagés depuis ma machine virtuelle que ce soit dans VirtualBox ou en ssh. 

Je me connecte à mon serveur samba de cette manière, smbclient agissant comme un client FTP et je liste ensuite les fichiers et dossiers de mon partage. 


![Accès au dossier partagé](https://i.imgur.com/4RAlw85.png)
